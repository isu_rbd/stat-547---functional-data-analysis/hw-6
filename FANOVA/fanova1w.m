function [pstat,params,gsize,vmu]=fanova1w(data,gparams)
%% [pstat,params,gsize, vmu]=fanova1w(data,gparams)
%% Homogeneous One-way ANOVA for functional data
%%  
%%
%%  data=[A,y1,y2,...,yp]: Nx(p+1)  data matrix
%%                           A: indicator for Factor A
%%   data matrx [y1,y2,...,yp]  each row = a discretized version of a function                        
%%
%%   gparams=[method,apprflag];
%%%% 
%%   method = 0    Pointwise F-test
%%          = 1    L2-norm based test
%%          = 2    F-type test
%%          = 3    Globalizing the pointwise F-test
%%
%%   apprflag = 0  naive method
%%            = 1  bias-reduced method
%% 
%% pstat=[stat,pvalue]
%% params=  [beta, (k-1)*kappa,kappa];  when method =1 or 3
%%          [(k-1)*kappa,(N-k)*kappa,kappa];  when method =2
%%          [k-1,N-k] when method =4
%% gsize---sample sizes of each cell
%% vmu=[mu1;mu2;...mua];
%% Jin-Ting Zhang
%% April 27, 2010, National University of Singapore, Singapore
%% Revised April 18, 2011, National University of Singapore, Singapore
%% Revised Oct 04, 2011, Princeton University, USA


[N,p]=size(data);
aflag=data(:,1);
yy=data(:,2:p);
dim=p-1; %% data dimension
aflag0=unique(aflag); %% Levels of Factor A
k=length(aflag0); %% Number of Factor A's levels
if nargin<2|length(gparams)==0,
    method =1; %% L2-norm based  test
    apprflag=0; %% naive method
elseif length(gparams)==1,
    method=gparams(1);
    apprflag=0;
elseif length(gparams)==2,
   method=gparams(1);apprflag=gparams(2); 
end


%% computing pointwise SSR, SSE 
mu0=mean(yy); %% sample grand mean function
gsize=[];SSR=0;SSE=0;vmu=[]; V=[];
for i=1:k,
    iflag=(aflag==aflag0(i));yyi=yy(iflag,:);
    ni=sum(iflag);gsize=[gsize;ni];
    mui=mean(yyi);
    SSR=SSR+ni*(mui-mu0).^2;  %% pointwise SSR
    Vi=yyi-ones(ni,1)*mui;
    SSE=SSE+sum(Vi.^2);  %% pointwise SSE
    vmu=[vmu;mui];V=[V;Vi];
end

if method==0,%% Pointwise F-test
   stat=SSR./SSE*(N-k)/(k-1);
   pvalue=1-fcdf(stat,k-1,N-k);
   pstat=[stat(:),pvalue(:)];
   params=[k-1,N-k];
end

if (method==1)|(method==2),
if N>p,
    V=V'*V/(N-k);  %% pxp pooled covariance matrix
else
    V=V*V'/(N-k);  %% Nx N matrix whch has the same non-zero eigen values with Sigma
end

A=trace(V);B=trace(V^2);
if apprflag==0, %% the naive method
    A2=A^2;B2=B;
elseif apprflag==1, %% the bias-reduced method
    A2=(N-k)*(N-k+1)/(N-k-1)/(N-k+2)*(A^2-2*B/(N-k+1));
    B2=(N-k)^2/(N-k-1)/(N-k+2)*(B-A^2/(N-k));
end

if method==1,%% L2-norm based test
    stat=sum(SSR);
    beta=B2/A;kappa=A2/B2;
    pvalue=1-chi2cdf(stat/beta,(k-1)*kappa);
    pstat=[stat,pvalue]; params=[beta,(k-1)*kappa,kappa];
elseif method==2, %% F-type test
    stat=sum(SSR)/sum(SSE)*(N-k)/(k-1);
    kappa=A2/B2;
    pvalue=1-fcdf(stat,(k-1)*kappa, (N-k)*kappa);
    pstat=[stat,pvalue]; params=[(k-1)*kappa,(N-k)*kappa,kappa];
end

end %% 

if method==3, %% %% Globalizing the pointwise F-test
    
    dd=sqrt(sum(V.^2));
    V=V./(ones(N,1)*dd);
    if N>=dim,
        V=V'*V;
    else
        V=V*V';
    end
    
    A=(N-k)/(N-k-2);
   
    B=trace(V^2)/dim^2/(k-1);
    
    stat=mean(SSR./SSE*(N-k)/(k-1));
    alpha=B/A;df=A^2/B;
    pvalue=1-chi2cdf(stat/alpha,df);
    pstat=[stat/alpha,pvalue]; params=[alpha,df,df/(k-1)];
end


    