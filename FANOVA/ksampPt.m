function [pstat,params]=ksampPt(yy,method,indfig)
%%function [pstat,params]=ksampPt(yy,method,indfig)
%% Pointwise tests for k-sample problem with a common cov funct.
%%   One-way ANOVA for functional data: Main-effects test
%% yy=[A,y1,y2,...,yp]  
%%   A: nx1  indicator of the levels of Factor A
%%  Matrix [y1,y2,...,yp]: nxp data matrix,  each row: discretization of a func.
%% 
%% method  = 1  Pointwise F-test, 
%%         = 2  Pointwise chi-squared test
%%         = [3, Nboot]  Pointwise bootstrap test
%%
%% indfig=0 plot the pointwise test
%%        =1 no plots
%%  Outputs
%%  pstat=[stat,pvalue], params=[df1,df2] when method =1
%%                       params=df1  when method=2
%%                       params= a bootstrap sample of statistic when
%%                       method=3
%%  Jin-Ting Zhang
%%  March 13, 2008; Revised July 11, 2008  NUS, Singapore
%%  Revised Oct 19, 2011 Princeton University

if nargin<2|length(method)==0,
    method=1;
elseif length(method)==1,
    if method==3,Nboot=1000;end
else
    method=method(1);Nboot=method(2);
end
if nargin<3|length(indfig)==0,indfig=0;end


%% Some basic calculations
[n,p]=size(yy);p=p-1;
aflag=yy(:,1);aflag0=unique(aflag); k=length(aflag0); %% Level of Factor A
yy=yy(:,2:(p+1)); %% nxp data matrix,  each row: discretization of a func.

mu0=mean(yy); %% pooled sample mean function
gsize=[];vmu=[];z=[];SSR=0;SSE=0;
for i=1:k,
   iflag=(aflag==aflag0(i));yi=yy(iflag,:); %%Samle i
   ni=size(yi,1);mui=mean(yi);zi=yi-ones(ni,1)*mui;
   gsize=[gsize;ni];vmu=[vmu;mui]; %% each row is a group mean vector
   z=[z;zi];
   SSR=SSR+ni*(mui-mu0).^2; %% 1xp vector
   SSE=SSE+sum((yi-ones(ni,1)*mui).^2); %%1xp vector
end



if method==1, %% Pointwise F-test
    
    stat=SSR'./SSE'*(n-k)/(k-1); params=[k-1,n-k];
    pvalue=1-fcdf(stat,k-1,n-k);
   pstat=[stat,pvalue];
elseif method==2, 
    stat=SSR'./SSE'*(n-k); params=k-1;
    pvalue=1-chi2cdf(stat,k-1);
   pstat=[stat,pvalue];
elseif method==3, %% bootstrapping method
   
    stat=SSR;
   for ii=1:Nboot,
      btvmu=[];
      for i=1:k,
          iflag=(aflag==aflag0(i));yi=yy(iflag,:);ni=gsize(i);
          btflag=fix(rand(ni,1)*(ni-1))+1; btyi=yi(btflag,:);
          btmui=mean(btyi);
          btmui=btmui-vmu(i,:);
          btvmu=[btvmu;btmui];
      end
      btmu0=gsize'*btvmu;
      
      btSSR=0;
      for i=1:k,
         btSSR=btSSR+gsize(i)*(btvmu(i,:)-btmu0).^2;
      end
    btstat(ii,:)=btSSR;
    
   end
   pvalue=mean(btstat>=(ones(Nboot,1)*stat));
   pstat=[stat',pvalue'];params=btstat;
end
 


