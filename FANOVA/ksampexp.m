%% Examples for one-way ANOVA for functional data
%%  for Section 3, Chapter 5
%%  Using the Canadian Temperature Data
%% Oct 19, 2011 Princeton University
%% Jin-Ting Zhang
%% Revised  Jan 8, 2013
%%  ksampexp.m
%%  call ksampL2.m
%%       ksampF.m
%%       ksampPt.m
%%       fanova1w.m
%%       tempfits.mat
%% Eaxmples 5.9-5.15 k-sample problem examples

cd '/Users/xdai/GoogleDrive/Teaching/ISU/STAT 547/codes/fanova';
pathstr='./';
datpath0=[pathstr,'./'];
eval(['load ',datpath0,'tempfits.mat'])
figpath=[pathstr,'./'];
datpath=[pathstr,'./'];


%% exflag=0; %%Trace computation
%% exflag=1;  %% Figures 5.5-5.7  suing ksampPt.m
%% exflag=2; %% Effect testing using ksampL2.m and ksampF.m 
%% exflag=3; %% Effect testing using fanova1w.m


labstr=['Day        '
        'Temperature'];  
   
    %% exflag=0; %% trace computation
    %exflag=1; %% plots
    %exflag=2; %% testing
    
    %%%
    eval(['diary  ', datpath,'ksampexp.txt'])
    disp('Examples of Section 3, Ch 5')
    disp('The Canadian Temperature data')
    
    disp(date)
    
    
    if exflag==0, %% Trace computation
       
    disp('Traces of \gamma(s,t) and \gamma^2(s,t): M=1000')
    y1=yfit(:,1:15)';y2=yfit(:,16:30)';y3=yfit(:,31:35)';
        n1=15;n2=15;n3=5;
    
    for testflag=1:5,
       
    if testflag==1,%% Whole year
         flag=(xfit>0)&(xfit<=365);
         disp('[0,365] Whole year')
    elseif testflag==2,%% Spring (March, April, May)
         flag=(xfit>59)&(xfit<=151);
         disp('[60,151] Spring (March, April, May)')
    elseif testflag==3,%%Summer (June, July, August)
         flag=(xfit>151)&(xfit<=243);
         disp('[152,243] Summer (June, July, August)')
    elseif testflag==4,%%Autumn (Sept. Oct, Nov)
         flag=(xfit>243)&(xfit<=334); 
         disp('[244,334] Autumn (Sept, Oct,Nov)')
    elseif testflag==5,%% Winter (Dec, January and Febrary)
         flag=((xfit>0)&(xfit<=59))|((xfit>334)&(xfit<=365));
         disp('[0,59] & [335,365] Winter (Dec, Jan., Feb.)')
    end
    yy1=y1(:,flag);yy2=y2(:,flag);yy3=y3(:,flag);
     covf1=cov(yy1);covf2=cov(yy2);covf3=cov(yy3);
     
    T11=trace(covf1);T12=trace(covf2);T13=trace(covf3);
    T21=sum(sum(covf1.^2));T22=sum(sum(covf2.^2));T23=sum(sum(covf3.^2));
    covf=((n1-1)*covf1+(n2-1)*covf2+(n3-1)*covf3)/(n1+n2+n3-3);
    
    T1=trace(covf);T2=sum(sum(covf.^2));
    
    disp(['[n1,n2,n3]=[',num2str([n1,n2,n3]),']'])
    
    disp(['Traces of gamma1,gamma2, gamma3=[',num2str([T11,T12, T13]),']'])
    disp(['Traces of gamma1^2,gamma2^2, gamma3^2=[',num2str([T21,T22,T23]),']'])
    disp(['Traces of gamma,gamma^2=[',num2str([T1,T2]),']'])
    
    end
    
   end
   
    
    %%%
    
    if exflag==1, %%Figures  5.5-5.7
       
        %% plot the group mean differences
        yy1=yfit(:,1:15)';yy2=yfit(:,16:30)';yy3=yfit(:,31:35)';
        n1=15;n2=15;n3=5;
        
        
        figure(1) %% plot the temperature curves
        plot(xfit,yy1(1,:),'k-','LineWidth', 1)
        hold on
        plot(xfit,yy2(1,:),'k--','LineWidth', 2)
        plot(xfit,yy3(1,:),'k-.','LineWidth', 3)
        % legend('East','West','North',0)
        
         plot(xfit,yy1,'k-','LineWidth', 1)
        plot(xfit,yy2,'k--','LineWidth', 2)
        plot(xfit,yy3,'k-.','LineWidth', 3)
        hold off
        title('Reconstructed individual functions')
        axis([0,365,-40,30])
        xlabel('Day'); ylabel('Temperature')
        eval(['print -depsc ',figpath,'KScurves.eps'])  %% Figure 5.5
        
       
        
        
        figure(2) %% Group mean functions with 95% CIs
        
        mu1=mean(yy1)';dd1=sqrt(diag(cov(yy1))/n1);
        mu2=mean(yy2)';dd2=sqrt(diag(cov(yy2))/n2);
        mu3=mean(yy3)';dd3=sqrt(diag(cov(yy3))/n3);
        plot(xfit,mu1,'k-','LineWidth',2)
        hold on
        plot(xfit,mu2,'k--','LineWidth',2)
        plot(xfit,mu3,'k-.','LineWidth',3)
        % legend('East','West','North',0)
        
        plot(xfit,mu1-1.96*dd1,'k-',xfit, mu1+1.96*dd1,'k-','LineWidth',2)
        plot(xfit,mu2-1.96*dd2,'k--',xfit, mu2+1.96*dd2,'k--','LineWidth',2)
        plot(xfit,mu3-1.96*dd3,'k-.',xfit, mu3+1.96*dd3,'k-.','LineWidth',3)
        title('Group mean functions with 95% pointwise CIs')
        hold off
        aa=axis;
        axis([0,365,aa(3),aa(4)])
        
        eval(['print -depsc ',figpath,'KSmeans.eps'])  %% Figure 5.7
        
        figure(3)
        subplot(2,1,1)
        yy=[[ones(n1,1),yy1];[2*ones(n2,1),yy2];[3*ones(n3,1),yy3]];
        pstatF=ksampPt(yy,1); %Pointwise   F-test
        plot(xfit,pstatF(:,2),'k-')
        xlabel('Day'); ylabel('P-value')
        aa=axis;axis([0,365,aa(3),aa(4)])
        title('(a) P-values of the pointwise F-test')
        
        subplot(2,1,2)
        pstatx2=ksampPt(yy,2);  %% Pointwise   chi-squared test
        plot(xfit,pstatx2(:,2),'k-')
        xlabel('Day'); ylabel('P-value')
        aa=axis;axis([0,365,aa(3),aa(4)])
        title('(b) P-values of the pointwise chi-squared test')
        
        eval(['print -depsc ',figpath,'KSpointF.eps'])  %% Figure 5.6
        
        
    end
    
    if exflag==2,  %% effect testing
        
    disp('Effect Testing using ksamF.m and ksamL2.m')
    
     format short e
for dflag=1:1,
    
    if dflag==1, %%  3 factor test
       A=[ones(1,15),2*ones(1,15),3*ones(1,5)]';
       yy0=yfit';
       disp('1-way ANOVA for East, West and North')
    elseif dflag==2, %% East vs West
       A=[ones(1,15),2*ones(1,15)]';
       yy0=yfit(:,[1:30])';
        disp('two-sample test for East vs West')
    elseif dflag==3,  %% East vs North
       A=[ones(1,15), 3*ones(1,5)]';
        disp('two-sample test for East vs North')
       yy0=yfit(:,[1:15,31:35])';
    elseif dflag==4,  %% West vs North
        A=[2*ones(1,15), 3*ones(1,5)]';
         disp('two-sample test for West vs North')
       yy0=yfit(:,[16:35])';
    end
        
        
    vpstat=[];vparam=[];
    for testflag=1:5,
       
        
    if testflag==1,%% Whole year
         flag=(xfit>0)&(xfit<=365);
         disp('[0,365] Whole year')
    elseif testflag==2,%% Spring (March, April, May)
         flag=(xfit>59)&(xfit<=151);
         disp('[60,151] Spring (March, April, May)')
    elseif testflag==3,%%Summer (June, July, August)
         flag=(xfit>151)&(xfit<=243);
         disp('[152,243] Summer (June, July, August)')
    elseif testflag==4,%%Autumn (Sept. Oct, Nov)
         flag=(xfit>243)&(xfit<=334); 
         disp('[244,334] Autumn (Sept, Oct,Nov)')
    elseif testflag==5,%% Winter (Dec, January and Febrary)
         flag=((xfit>0)&(xfit<=59))|((xfit>334)&(xfit<=365));
         disp('[0,59] & [335,365] Winter (Dec, Jan., Feb.)')
    end
    
    yy=[A,yy0(:,flag)];
    [pstat11,param11]=ksampL2(yy,[1,0], 0);  %% L2, naive
    [pstat12,param12]=ksampL2(yy,[1,0], 1);  %% L2, bias-reduced
    pstat13=ksampL2(yy,[3,10000]); %% L2, bootstrap
    
    [pstat21,param21]=ksampF(yy,[1,0], 0);  %% F, naive
    [pstat22,param22]=ksampF(yy,[1,0], 1);  %% F, bias-reduced
    pstat23=ksampF(yy,[2,10000]); %% F, bootstrap
    
    pstat=[pstat11;pstat12;pstat21;pstat22;pstat13;pstat23];
    disp('stat pvalue')
    disp('Column: [Whole, Spring, Summer, Autumn, Winter')
    disp('Row: each row associated with L2-norm (Naive, B-reduc.), F-type(Naive, B-reduc.), Bootstrap test (L2, F)')
    disp('pstat')
    disp(pstat)
    
    param=[param11;param12;param21;param22];
    disp('param')
     disp('[beta, d, kappa] or [d1,d2,kappa]')
    disp(param)
    
    vpstat=[vpstat,pstat];
    vparam=[vparam,param];
    end
    
   
    disp('stat pvalue')
    disp('Column: [Whole, Spring, Summer, Autumn, Winter')
    disp('Row: each row associated with L2-norm (Naive, B-reduc.), F-type(Naive, B-reduc.), Bootstrap test (L2, F)')
    disp('stat')
    disp(vpstat(:,1:2:9))
    disp('pvalue')
    disp(vpstat(:,2:2:10))
    
     disp('[beta, d, kappa] or [d1,d2,kappa]')
    disp('Column: [Whole, Spring, Summer, Autumn, Winter (each for 3 columns]')
    disp('Row: each row associated with L2-norm(Naive, B-reduc.), F-type  test(Naive, B-reduc.)')
    disp(vparam)
end
format
   
    end
   
    
    if exflag==3,  %% effect testing
        
    disp('Effect Testing using fanova1w.m')
    
     format short e
for dflag=1:1,
    
    if dflag==1, %%  3 factor test
       A=[ones(1,15),2*ones(1,15),3*ones(1,5)]';
       yy0=yfit';
       disp('1-way ANOVA for East, West and North')
    elseif dflag==2, %% East vs West
       A=[ones(1,15),2*ones(1,15)]';
       yy0=yfit(:,[1:30])';
        disp('two-sample test for East vs West')
    elseif dflag==3,  %% East vs North
       A=[ones(1,15), 3*ones(1,5)]';
        disp('two-sample test for East vs North')
       yy0=yfit(:,[1:15,31:35])';
    elseif dflag==4,  %% West vs North
        A=[2*ones(1,15), 3*ones(1,5)]';
         disp('two-sample test for West vs North')
       yy0=yfit(:,[16:35])';
    end
        
        
    vpstat=[];vparam=[];
    for testflag=1:5,
       
        
    if testflag==1,%% Whole year
         flag=(xfit>0)&(xfit<=365);
         disp('[0,365] Whole year')
    elseif testflag==2,%% Spring (March, April, May)
         flag=(xfit>59)&(xfit<=151);
         disp('[60,151] Spring (March, April, May)')
    elseif testflag==3,%%Summer (June, July, August)
         flag=(xfit>151)&(xfit<=243);
         disp('[152,243] Summer (June, July, August)')
    elseif testflag==4,%%Autumn (Sept. Oct, Nov)
         flag=(xfit>243)&(xfit<=334); 
         disp('[244,334] Autumn (Sept, Oct,Nov)')
    elseif testflag==5,%% Winter (Dec, January and Febrary)
         flag=((xfit>0)&(xfit<=59))|((xfit>334)&(xfit<=365));
         disp('[0,59] & [335,365] Winter (Dec, Jan., Feb.)')
    end
    
    yy=[A,yy0(:,flag)];
    [pstat11,param11]=fanova1w(yy,[1,0]);  %% method=1,L2, apprflag=0,naive
    [pstat12,param12]=fanova1w(yy,[1,1]);  %% L2, bias-reduced
    
    [pstat21,param21]=fanova1w(yy,[2,0]);  %% F, naive
    [pstat22,param22]=fanova1w(yy,[2,1]);  %% F, bias-reduced
   
    [pstat31,param31]=fanova1w(yy,[3,0]);  %% GPF, naive
    
    pstat=[pstat11;pstat12;pstat21;pstat22;pstat31];
    disp('stat pvalue')
    disp('Column: [Whole, Spring, Summer, Autumn, Winter')
    disp('Row: each row associated with L2-norm (Naive, B-reduc.), F-type(Naive, B-reduc.), GPF test (naive)')
    disp('pstat')
    disp(pstat)
    
    param=[param11;param12;param21;param22];
    disp('param')
     disp('[beta, d, kappa] or [d1,d2,kappa]')
    disp(param)
    
    vpstat=[vpstat,pstat];
    vparam=[vparam,param];
    end
    
   
    disp('stat pvalue')
    disp('Column: [Whole, Spring, Summer, Autumn, Winter')
    disp('Row: each row associated with L2-norm (Naive, B-reduc.), F-type(Naive, B-reduc.), Bootstrap test (L2, F)')
    disp('stat')
    disp(vpstat(:,1:2:9))
    disp('pvalue')
    disp(vpstat(:,2:2:10))
    
     disp('[beta, d, kappa] or [d1,d2,kappa]')
    disp('Column: [Whole, Spring, Summer, Autumn, Winter (each for 3 columns]')
    disp('Row: each row associated with L2-norm(Naive, B-reduc.), F-type  test(Naive, B-reduc.)')
    disp(vparam)
end
format
   
   end
    
 diary off

